# TypeScript Microservices

This repository contains notes and code from studying [TypeScript Microservices](https://www.packtpub.com/application-development/typescript-microservices).

Projects:

- [Hello TypeScript Microservices](./code/hello_ts_microservice)

