# Hello world typescript microservice example

This folder contains example "Hello world" microservice written in TypeScript.

## Service example

The service exposes only one command `GET hello-world` without any parameters.

Example request/response:

```bash
curl -vk http://192.168.0.16:8080/hello-world

*   Trying 192.168.0.16...
* TCP_NODELAY set
* Connected to 192.168.0.16 (192.168.0.16) port 8080 (#0)
> GET /hello-world HTTP/1.1
> Host: 192.168.0.16:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Access-Control-Allow-Origin: *
< Content-Type: application/json; charset=utf-8
< Content-Length: 47
< ETag: W/"2f-PTnVycPuOxVmxvQaPuKYsgwLBUk"
< Date: Sun, 18 Nov 2018 23:05:32 GMT
< Connection: keep-alive
< 
* Connection #0 to host 192.168.0.16 left intact

{"msg":"This is first Typescript Microservice"}
```

## NPM Modules

The project uses the following npm modules:

- [CORS](https://www.npmjs.com/package/cors): Adds CORS headers, so that cross applications can access it.

- [Routing Controllers](https://www.npmjs.com/package/routing-controllers): A decoration provider, which helps to write API's and routes easily.

- [Winston](https://www.npmjs.com/package/winston): Logging module with many advanced features.

## Project initialization

The project was initialized using the following steps:

```bash
npm init

npm install body-parser config cookie-parser cors debug express reflect-metadata rimraf routing-controllers typescript winston --save

npm install @types/cors @types/config @types/debug @types/node @types/body-parser @types/express @types/winston --only=dev
```

The output build of TypeScript from `./src` goes to JavaScript output will go to `./dist`. Check the source code and the `tsconfig.json` file.

**Test** the the build with `npm run build`.

**Run** the service with `npm run start`.

The web service is accessible on port _3000_, in my case: `http://192.168.0.16:3000/hello-world`.

## Docker image

Docker image was created as bellow (note: it should end with the dot at the end):

```bash
sudo docker build -t firstypescriptms .
```

To **run the Docker image** type the following command (note: Option `-d` de-attaches the Docker process):

```bash
sudo docker run -p 8080:3000 -d firstypescriptms:latest
```

The Docker service should be running on port _8080_, in my case: `http://192.168.0.16:8080/hello-world`.
