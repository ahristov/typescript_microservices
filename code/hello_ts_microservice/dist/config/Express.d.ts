import * as express from 'express';
export declare class ExpressConfig {
    app: express.Express;
    constructor();
    setUpControllers(): void;
}
